### Tinpet

## Description
Il s'agit d'une application permettant de trouver un nouvel animal de companie avec une interface s'inspirant de tinder. Il n'y a pas d'api donc il n'est possible que de voir les différentes fonctionalités

## Comment installer
* installer expo sur votre téléphone portable [ios](https://apps.apple.com/app/apple-store/id982107779) ou [android](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www)
* visiter ce [lien](https://exp.host/@alexg2/tinpet)
* scannez le QR code présent sur la page
* enjoy :)