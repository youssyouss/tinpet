import * as firebase from 'firebase';


const firebaseConfig = {
  apiKey: 'AIzaSyBvAS0sMwT5Q9WMe0hzwtyVWQ1olHa9ykQ',
  authDomain: 'tinpet-682d1.firebaseapp.com',
  databaseURL: 'https://tinpet-682d1.firebaseio.com',
  projectId: 'tinpet-682d1',
  storageBucket: 'tinpet-682d1.appspot.com',
  messagingSenderId: '83896990192',
  appId: '1:83896990192:web:ee9f13437fbdf5c47dc535',
};


// Initialize Firebase
firebase.initializeApp(firebaseConfig);


export default firebase;
