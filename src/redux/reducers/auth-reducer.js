import { authTypes } from '../actions/auth-action';

const initialState = {
  authMsg: '',
  isLoggedIn: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case authTypes.SIGNIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        authMsg: '',
      };
    case authTypes.SIGNOUT_SUCCESS:
      return {
        ...state,
        isLoggedIn: false,
        authMsg: '',
      };
    case authTypes.SIGNUP_SUCCESS:
      return {
        ...state,
        isLoggedIn: false,
        authMsg: action.payload,
      };
    case authTypes.SIGNUP_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        authMsg: action.payload,
      };
    case authTypes.SIGNOUT_ERROR:
      return {
        ...state,
        isLoggedIn: true,
        authMsg: action.payload,
      };
    default:
      return state;
  }
};

export default authReducer;
