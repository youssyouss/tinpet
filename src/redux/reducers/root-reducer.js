import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase';
import authReducer from './auth-reducer';

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  authState: authReducer,
});

export default rootReducer;
