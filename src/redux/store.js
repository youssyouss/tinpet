import { createStore, compose, applyMiddleware } from 'redux';
import { getFirebase } from 'react-redux-firebase';
// import applyAppStateListener from 'redux-enhancer-react-native-appstate';
import thunk from 'redux-thunk';
import rootReducer from './reducers/root-reducer';

const configureStore = () => {
  const middleware = [
    thunk.withExtraArgument({ getFirebase }),
  ];
  const createStoreWithMiddleware = compose(
    applyMiddleware(...middleware),
  )(createStore);
  const store = createStoreWithMiddleware(rootReducer);

  return store;
};

export default configureStore;
