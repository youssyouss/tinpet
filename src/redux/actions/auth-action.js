import firebase from '../../config/firebase.config';

export const authTypes = {
  SET_CURRENT_USER: 'SET_CURRENT_USER',
  CLEAR_CURRENT_USER: 'CLEAR_CURRENT_USER',
  AUTH_SUCCESS: 'AUTH_SUCCESS',
  AUTH_ERROR: 'AUTH_ERROR',
  SIGNUP_SUCCESS: 'SIGNUP_SUCCES',
  SIGNUP_ERROR: 'SIGNUP_ERROR',
  SIGNIN_SUCCESS: 'SIGNIN_SUCCESS',
  SIGNIN_ERROR: 'SIGNIN_ERROR',
  SIGNOUT_SUCCESS: 'SIGNOUT_SUCCESS',
  SIGNOUT_ERROR: 'SIGNOUT_ERROR',
};

// firebase signup
export const signUp = (email, password, name) => (dispatch) => {
  try {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        firebase.auth().onAuthStateChanged((user) => {
          user.sendEmailVerification();
        });
      })
      .then((userCredentials) => userCredentials.user.updateProfile({
        displayName: name,
      }))
      .then(() => {
        firebase.auth().onAuthStateChanged((user) => {
          if (user.emailVerified) {
            dispatch({
              type: authTypes.SIGNUP_SUCCESS,
              payload: 'Your account has been successfully created! Please verify your email',
            });
          } else {
            dispatch({
              type: authTypes.SIGNUP_ERROR,
              payload: 'Something went wrong ...',
            });
          }
        });
      })
      .catch(() => {
        dispatch({
          type: authTypes.SIGNUP_ERROR,
          payload: 'Sorry, There was an error while Signing up ...',
        });
      });
  } catch (err) {
    dispatch({
      type: authTypes.SIGNUP_ERROR,
      payload: 'Sorry, There was an error while Signing up ...',
    });
  }
};


// firebase Signin

export const signIn = (email, password) => (dispatch) => {
  try {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        dispatch({ type: authTypes.SIGNUP_SUCCESS });
      })
      .catch(() => {
        dispatch({
          type: authTypes.SIGNIN_ERROR,
          payload: 'Invalid login',
        });
      });
  } catch (err) {
    dispatch({
      type: authTypes.SIGNIN_ERROR,
      payload: 'Invalid login',
    });
  }
};

// SignOut with firebase

export const signout = () => (dispatch) => {
  try {
    firebase
      .auth()
      .signOut()
      .then(() => {
        dispatch({
          type: authTypes.SIGNOUT_SUCCESS,
        });
      })
      .catch(() => {
        dispatch({
          type: authTypes.SIGNOUT_ERROR,
          payload: 'Error while signing out',
        });
      });
  } catch (err) {
    dispatch({
      type: authTypes.SIGNOUT_ERROR,
      payload: 'Error while signing out',
    });
  }
};
