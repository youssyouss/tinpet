import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity, Image, StatusBar, LayoutAnimation, KeyboardAvoidingView,
} from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavigateActions } from 'react-navigation';
import { signIn } from '../../redux/actions/auth-action';

const authHeader = require('../../../assets/authHeader.png');
const loginLogo = require('../../../assets/loginlogo.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  greeting: {
    marginTop: 32,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  errorMessage: {
    height: 72,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30,
  },
  error: {
    color: '#E9446A',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center',
  },
  form: {
    marginBottom: 48,
    marginHorizontal: 30,
  },
  inputTitle: {
    color: '#8A8F9E',
    fontSize: 10,
    textTransform: 'uppercase',
  },
  input: {
    borderBottomColor: '#8A8F9E',
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: 40,
    fontSize: 15,
    color: '#161F3D',
  },
  button: {
    marginHorizontal: 30,
    backgroundColor: '#E9446A',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },

});

const mapStateToProps = (state) => ({
  authMsg: state.authMsg,
  isLoggedIn: state.isLoggedIn,
});

const mapDispatchToProps = {
  signin: signIn,
};

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      errorMessage: null,
    };
  }

  loginSuccess = () => {
    const { isLoggedIn } = this.props;
    if (isLoggedIn === true) { NavigateActions.NAVIGATE('Home'); }
  };

  handleLogin = (e) => {
    const { signin } = this.props;

    e.preventDefault();
    const { email, password } = this.state;

    signin(email, password);
    this.loginSuccess();
  };

  static navigationOptions = {
    headerShown: false,
  };

  render() {
    const { navigation } = this.props;
    const {
      errorMessage,
      email,
      password,
    } = this.state;

    LayoutAnimation.easeInEaseOut();

    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <Image
          source={authHeader}
          style={{ marginTop: -176, marginLeft: -50 }}
        />
        <Image
          source={authHeader}
          style={{ position: 'absolute', bottom: -280, right: -225 }}
        />
        <Image
          source={loginLogo}
          style={{
            marginTop: -110, alignSelf: 'center', width: 70, height: 60,
          }}
        />
        <Text style={styles.greeting}>
          {'Hello again. \n Welcome back.'}
        </Text>
        <View style={styles.errorMessage}>
          {errorMessage && <Text style={styles.error}>{errorMessage}</Text>}
        </View>
        <View style={styles.form}>
          <View>
            <Text style={styles.inputTitle}>Email</Text>
            <KeyboardAvoidingView enabled behavior="height" keyboardVerticalOffset={20}>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={(newEmail) => this.setState({ email: newEmail })}
                value={email}
              />
            </KeyboardAvoidingView>
          </View>
          <View style={{ marginTop: 32 }}>
            <Text style={styles.inputTitle}>Password</Text>
            <TextInput
              style={styles.input}
              autoCapitalize="none"
              onChangeText={(newPassword) => this.setState({ password: newPassword })}
              value={password}
              secureTextEntry
            />
          </View>
        </View>
        <TouchableOpacity style={styles.button} onPress={this.handleLogin}>
          <Text style={{ color: '#fff', fontWeight: '500' }}>Sign in</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ alignSelf: 'center', marginTop: 32 }} onPress={() => navigation.navigate('SignUp')}>
          <Text style={{ color: '#414959', fontSize: 13 }}>
                  New to Tinpet ?
            {' '}
            <Text style={{ color: '#E9446A', fontWeight: '500' }}>Sign up</Text>
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

LoginScreen.propTypes = {
  navigation: PropTypes.shape({ goBack: PropTypes.func, navigate: PropTypes.func }).isRequired,
  isLoggedIn: PropTypes.bool,
  signin: PropTypes.func,
};

LoginScreen.defaultProps = {
  isLoggedIn: false,
  signin: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
