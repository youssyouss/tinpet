import React from 'react';
import {
  View, Text, StyleSheet, TextInput, TouchableOpacity, Image, StatusBar, LayoutAnimation,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


import { signUp } from '../../redux/actions/auth-action';

const authHeader = require('../../../assets/authHeader.png');
const loginLogo = require('../../../assets/loginlogo.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  greeting: {
    marginTop: 32,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  errorMessage: {
    height: 72,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30,
  },
  error: {
    color: '#E9446A',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center',
  },
  form: {
    marginBottom: 48,
    marginHorizontal: 30,
  },
  inputTitle: {
    color: '#8A8F9E',
    fontSize: 10,
    textTransform: 'uppercase',
  },
  input: {
    borderBottomColor: '#8A8F9E',
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: 40,
    fontSize: 15,
    color: '#161F3D',
  },
  button: {
    marginHorizontal: 30,
    backgroundColor: '#E9446A',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  back: {
    position: 'absolute',
    top: 48,
    left: 32,
    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: 'rgba(21,22,48,0.1)',
    alignItems: 'center',
    justifyContent: 'center',
  },

});

const mapStateToProps = (state) => ({
  authMsg: state.authMsg,
});

const mapDispatchToProps = {
  signup: signUp,
};

class SignUpScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      password: '',
      errorMessage: null,
    };
  }

  handleSignUp = () => {
    const { email, password, name } = this.state;
    const { signup } = this.props;

    signup(email, password, name);
  }

  static navigationOptions = {
    headerShown: false,
  };


  render() {
    const { navigation } = this.props;
    const {
      errorMessage,
      name,
      email,
      password,
    } = this.state;

    LayoutAnimation.easeInEaseOut();

    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <Image
          source={authHeader}
          style={{ marginTop: -176, marginLeft: -50 }}
        />
        <Image
          source={authHeader}
          style={{ position: 'absolute', bottom: -280, right: -225 }}
        />
        <Image
          source={loginLogo}
          style={{
            marginTop: -110, alignSelf: 'center', width: 70, height: 60,
          }}
        />
        <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
          <Ionicons name="ios-arrow-round-back" size={32} color="#fff" />
        </TouchableOpacity>
        {/* <View style={{position: "absolute", top: 64, alignItems: "center", width: "100%"}}>
          </View> */}
        <Text style={styles.greeting}>
          {'Hello! \n Sign up to get started.'}
        </Text>
        <View style={styles.errorMessage}>
          {errorMessage && <Text style={styles.error}>{errorMessage}</Text>}
        </View>
        <View style={styles.form}>
          <View>
            <Text style={styles.inputTitle}>Full name</Text>
            <TextInput
              style={styles.input}
              autoCapitalize="none"
              onChangeText={(newName) => this.setState({ name: newName })}
              value={name}
            />
          </View>
          <View style={{ marginTop: 32 }}>
            <Text style={styles.inputTitle}>Email</Text>
            <TextInput
              style={styles.input}
              autoCapitalize="none"
              onChangeText={(newEmail) => this.setState({ email: newEmail })}
              value={email}
            />
          </View>
          <View style={{ marginTop: 32 }}>
            <Text style={styles.inputTitle}>Password</Text>
            <TextInput
              style={styles.input}
              autoCapitalize="none"
              onChangeText={(newPassword) => this.setState({ password: newPassword })}
              value={password}
              secureTextEntry
            />
          </View>
        </View>
        <TouchableOpacity style={styles.button} onPress={this.handleSignUp}>
          <Text style={{ color: '#fff', fontWeight: '500' }}>Sign Up</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ alignSelf: 'center', marginTop: 32 }} onPress={() => navigation.navigate('Login')}>
          <Text style={{ color: '#414959', fontSize: 13 }}>
                  Already have an account ?
            {' '}
            <Text style={{ color: '#E9446A', fontWeight: '500' }}>Sign in</Text>
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

SignUpScreen.propTypes = {
  navigation: PropTypes.shape({ goBack: PropTypes.func, navigate: PropTypes.func }).isRequired,
  signup: PropTypes.func,
};

SignUpScreen.defaultProps = {
  signup: () => { },
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
