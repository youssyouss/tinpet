import React from 'react';
import {
  View, Text, StyleSheet, ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';

import * as firebase from 'firebase';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


export default class LoadingScreen extends React.Component {
  componentDidMount() {
    const { navigation } = this.props;

    firebase.auth().onAuthStateChanged((user) => {
      navigation.navigate(user ? 'App' : 'Auth');
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Loading ...</Text>
        <ActivityIndicator size="large" />
      </View>
    );
  }
}

LoadingScreen.propTypes = {
  navigation: PropTypes.shape({ goBack: PropTypes.func, navigate: PropTypes.func }).isRequired,
};
