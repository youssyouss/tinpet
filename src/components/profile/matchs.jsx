import React, { useState } from 'react';
import {
  StyleSheet, View, Text, FlatList, ActivityIndicator,
} from 'react-native';
import { Image, Icon } from 'react-native-elements';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2fcf7',
  },
  noMatchMessageContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#f2fcf7',
  },
  matchContainer: {
    flex: 1,
    padding: 15,
    borderRadius: 5,
    backgroundColor: 'white',
    margin: 15,
    marginBottom: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});

const placeholderMatchList = [
  {
    id: '1',
    name: 'Fred le lapin',
    picture: 'https://argos-veterinaire.com/wp-content/uploads/2017/04/Fiche-pratique-lapin-vétérinaire-Bordeaux.jpg',
  },
  {
    id: '2',
    name: 'Zapo le chat',
    picture: 'https://www.sciencesetavenir.fr/assets/img/2017/03/29/cover-r4x3w1000-58dbbd655242b-capture-d-e-cran-2017-03-29-a-15-55-40.png',
  },
  {
    id: '3',
    name: 'Lassie le serpent',
    picture: 'https://geo.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fgeo.2F2019.2F10.2F24.2Fe490e97f-cbe2-467d-95d9-33a821b4723a.2Ejpeg/640x480/cr/wqkgUGl4YWJheSAvIEdFTw%3D%3D/picture.jpg',
  },
];


const Matchs = () => {
  // match -> {id: string, name: string, picture: string (link)}
  const [matchList, setmatchList] = useState(placeholderMatchList);

  const deleteMatch = (match) => {
    const newMatchList = matchList.filter((m) => m.id !== match.id);
    setmatchList(newMatchList);
  };

  const RenderMatch = ({ item }) => (
    <View style={styles.matchContainer}>
      <Image
        source={{ uri: item.picture }}
        style={{ width: '100%', height: 200 }}
        PlaceholderContent={<ActivityIndicator />}
      />
      <Text style={{ marginTop: 10 }}>{item.name}</Text>
      <View style={{ position: 'absolute', bottom: 5, right: 5 }}>
        <Icon
          name="delete"
          color="#e87476"
          size={30}
          onPress={() => { deleteMatch(item); }}
        />
      </View>
    </View>
  );

  RenderMatch.propTypes = {
    item: PropTypes.shape({
      picture: PropTypes.string,
      name: PropTypes.string,
      id: PropTypes.string,
    }).isRequired,
  };


  if (matchList.length === 0) {
    return (
      <View style={styles.noMatchMessageContainer}>
        <Text style={{ textAlign: 'center' }}>Pas de matchs :(</Text>
      </View>
    );
  }

  return (
    <View>
      <FlatList
        data={matchList}
        keyExtractor={(item) => item.id}
        renderItem={RenderMatch}
        style={{ backgroundColor: '#f2fcf7' }}
      />
    </View>
  );
};

export default Matchs;
