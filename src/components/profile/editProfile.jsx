import React, { useState } from 'react';
import {
  View, StyleSheet, Text, TextInput, ScrollView, KeyboardAvoidingView, TouchableOpacity,
} from 'react-native';
import { Avatar, Icon } from 'react-native-elements';
import PropTypes from 'prop-types';
import * as ImagePicker from 'expo-image-picker';
import DropdownAlert from 'react-native-dropdownalert';
import * as firebase from 'firebase';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2fcf7',
  },
  avatarContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
  validateContainer: {
    padding: 10,
    width: '70%',
    backgroundColor: '#e87476',
    marginBottom: 15,
    borderRadius: 25,
  },
  imageButton: {
    marginTop: 10,
    flexDirection: 'row',
  },
  headerTextStyle: {
    marginTop: 10,
    width: '70%',
    color: '#b3b5b3',
  },
  textInputStyle: {
    width: '70%',
    borderBottomWidth: 1,
    borderBottomColor: '#b3b5b3',
  },
});
const EditProfile = (props) => {
  const [username, setusername] = useState(firebase.auth().currentUser.displayName);
  const [email, setemail] = useState(firebase.auth().currentUser.email);
  const [avataruri, setavataruri] = useState('https://www.recia.fr/wp-content/uploads/2018/10/default-avatar-300x300.png');
  let dropDownAlertRef;


  const handleValidate = async () => {
    try {
      if (username && username !== '') {
        await firebase.auth().currentUser.updateProfile({ displayName: username });
      }
      if (email && email !== '') {
        await firebase.auth().currentUser.updateEmail(email);
      }
    } catch (error) {
      dropDownAlertRef.alertWithType('error', 'Error', 'Impossible de mettre a jour vos informations');
    }
    props.navigation.goBack();
  };

  const handleOnGalleryButtonClick = async () => {
    const perm = await ImagePicker.requestCameraRollPermissionsAsync();
    if (!perm.granted) {
      return;
    }

    const opt = {};
    const res = await ImagePicker.launchImageLibraryAsync(opt);
    if (res.cancelled) {
      dropDownAlertRef.alertWithType('error', 'Error', "Impossible d'acceder à la gallerie");
      return;
    }
    setavataruri(res.uri);
  };

  const handleOnCameraButtonClick = async () => {
    const perm = await ImagePicker.requestCameraPermissionsAsync();
    if (!perm.granted) {
      return;
    }

    const opt = {};
    const res = await ImagePicker.launchCameraAsync(opt);
    if (res.cancelled) {
      dropDownAlertRef.alertWithType('error', 'Error', "Impossible d'acceder à la camera");
      return;
    }
    setavataruri(res.uri);
  };

  const isValidEmail = () => email.length === 0 || RegExp("^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+.[a-zA-Z]+").test(email);

  return (
    <KeyboardAvoidingView style={styles.container} enabled behavior="height">
      <ScrollView>
        <View style={styles.avatarContainer}>
          <Avatar
            rounded
            size={200}
            source={{
              uri: avataruri,
            }}
          />
          <View style={styles.imageButton}>
            <View>
              <Icon
                color="#e87476"
                size={35}
                name="photo"
                onPress={() => handleOnGalleryButtonClick()}
              />
            </View>
            <View style={{ marginLeft: '15%' }}>
              <Icon
                color="#e87476"
                size={35}
                name="photo-camera"
                onPress={() => { handleOnCameraButtonClick(); }}
              />
            </View>
          </View>

          <Text style={[styles.headerTextStyle, { marginTop: 30 }]}>Nom</Text>
          <TextInput
            style={styles.textInputStyle}
            onChangeText={(text) => setusername(text)}
            value={username}
          />
          <Text style={styles.headerTextStyle}>Email</Text>
          <TextInput
            style={[styles.textInputStyle, { marginBottom: 50, borderBottomColor: isValidEmail() ? styles.textInputStyle.borderBottomColor : 'red' }]}
            onChangeText={(text) => setemail(text)}
            value={email}
          />
          <View style={[styles.validateContainer, { backgroundColor: isValidEmail() ? styles.validateContainer.backgroundColor : 'grey' }]}>
            <TouchableOpacity onPress={() => { handleValidate(); }}>
              <Text style={{ textAlign: 'center', color: 'white' }}>Valider</Text>
            </TouchableOpacity>
          </View>
        </View>


      </ScrollView>
      <DropdownAlert ref={(ref) => { dropDownAlertRef = ref; }} />
    </KeyboardAvoidingView>
  );
};

EditProfile.propTypes = {
  navigation: PropTypes.shape({ goBack: PropTypes.func }).isRequired,
};

export default EditProfile;
