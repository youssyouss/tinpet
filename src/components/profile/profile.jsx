import React from 'react';
import {
  View, StyleSheet, Text, TouchableOpacity,
} from 'react-native';
import { Avatar, Icon } from 'react-native-elements';
import * as firebase from 'firebase';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2fcf7',
  },
  avatarContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
  disconectContainer: {
    position: 'absolute',
    bottom: 0,
    left: '15%',
    padding: 10,
    width: '70%',
    marginBottom: 15,
    borderRadius: 25,
  },
  textContainer: {
    marginTop: 10,
  },
  headerRight: {
    marginRight: 15,
  },
  disconnectTextStyle: {
    textAlign: 'center',
    color: 'red',
    textDecorationLine: 'underline',
    fontSize: 10,
  },
  myMatchs: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    marginBottom: 50,
    backgroundColor: '#e87476',
    padding: 10,
    borderRadius: 25,
  },
});

const Profile = ({ navigation }) => {
  const handleDisconect = () => {
    firebase.auth().signOut();
  };

  const handleMyMatchPress = () => {
    navigation.navigate('Matchs');
  };

  const username = firebase.auth().currentUser.email;

  return (
    <View style={styles.container}>
      <View style={styles.avatarContainer}>
        <Avatar
          rounded
          size={200}
          source={{
            uri: 'https://www.recia.fr/wp-content/uploads/2018/10/default-avatar-300x300.png',
          }}
        />
        <View style={styles.textContainer}>
          <Text style={{ fontSize: 20 }}>{username}</Text>
        </View>
      </View>


      <View style={styles.disconectContainer}>
        <TouchableOpacity style={styles.myMatchs} onPress={() => { handleMyMatchPress(); }}>
          <Text style={{ color: 'white' }}>Mes matchs</Text>
          <View style={{ position: 'absolute', right: 20, top: 8 }}>
            <Icon
              name="arrow-forward"
              color="white"
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { handleDisconect(); }}>
          <Text style={styles.disconnectTextStyle}>Déconnexion</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

Profile.navigationOptions = ({ navigation }) => ({
  headerRight: () => (
    <View style={styles.headerRight}>
      <Icon
        name="edit"
        onPress={() => { navigation.navigate('EditProfile'); }}
      />
    </View>
  ),
});

Profile.propTypes = {
  navigation: PropTypes.shape({ navigate: PropTypes.func }).isRequired,
};

export default Profile;
