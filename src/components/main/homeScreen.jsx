import React from 'react';
import {
  View, Text, Image, Dimensions, Animated, PanResponder, StyleSheet,
} from 'react-native';
import { Icon } from 'react-native-elements';


const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
const dogImg = require('../../../assets/Chat.jpg');
const catImg = require('../../../assets/chien.jpg');
const rabbitImg = require('../../../assets/lapin.jpg');

const Match = [
  { id: '1', uri: catImg },
  { id: '2', uri: dogImg },
  { id: '3', uri: rabbitImg },
  { id: '4', uri: catImg },
  { id: '5', uri: dogImg },
  { id: '6', uri: rabbitImg },
  { id: '7', uri: catImg },
  { id: '8', uri: dogImg },
  { id: '9', uri: rabbitImg },
];

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  content: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    width: 320,
    height: 470,
    backgroundColor: '#FE474C',
    borderRadius: 5,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
  },
  card1: {
    backgroundColor: '#FE474C',
  },
  card2: {
    backgroundColor: '#FEB12C',
  },
  label: {
    lineHeight: 400,
    textAlign: 'center',
    fontSize: 55,
    fontFamily: 'System',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    width: 220,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 0,
  },
  orange: {
    width: 55,
    height: 55,
    borderWidth: 6,
    borderColor: 'rgb(246,190,66)',
    borderRadius: 55,
    marginTop: -15,
  },
  green: {
    width: 75,
    height: 75,
    backgroundColor: '#fff',
    borderRadius: 75,
    borderWidth: 6,
    borderColor: '#01df8a',
  },
  red: {
    width: 75,
    height: 75,
    backgroundColor: '#fff',
    borderRadius: 75,
    borderWidth: 6,
    borderColor: '#fd267d',
  },
  button1: {
    marginHorizontal: 30,
    backgroundColor: '#E9446A',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 32,
    height: 40,
  },
  greeting: {
    marginTop: 22,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  headerRight: {
    marginRight: 15,
  },
});


export default class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerRight: () => (
      <View style={styles.headerRight}>
        <Icon
          name="account-box"
          size={25}
          onPress={() => { navigation.navigate('Profile'); }}
        />
      </View>
    ),
  })

  constructor() {
    super();

    this.position = new Animated.ValueXY();
    this.state = {
      currentIndex: 0,
    };

    this.rotate = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: ['-10deg', '0deg', '10deg'],
      extrapolate: 'clamp',
    });

    this.rotateAndTranslate = {
      transform: [{
        rotate: this.rotate,
      },
      ...this.position.getTranslateTransform(),
      ],
    };

    this.likeOpacity = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: [0, 0, 1],
      extrapolate: 'clamp',
    });
    this.dislikeOpacity = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: [1, 0, 0],
      extrapolate: 'clamp',
    });

    this.nextCardOpacity = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: [1, 0, 1],
      extrapolate: 'clamp',
    });
    this.nextCardScale = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: [1, 0.8, 1],
      extrapolate: 'clamp',
    });
  }

  componentWillMount() {
    const { currentIndex } = this.state;

    this.PanResponder = PanResponder.create({

      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (_, gestureState) => {
        this.position.setValue({ x: gestureState.dx, y: gestureState.dy });
      },
      onPanResponderRelease: (_, gestureState) => {
        if (gestureState.dx > 120) {
          Animated.spring(this.position, {
            toValue: { x: SCREEN_WIDTH + 100, y: gestureState.dy },
          }).start(() => {
            this.setState({ currentIndex: currentIndex + 1 }, () => {
              this.position.setValue({ x: 0, y: 0 });
            });
          });
        } else if (gestureState.dx < -120) {
          Animated.spring(this.position, {
            toValue: { x: -SCREEN_WIDTH - 100, y: gestureState.dy },
          }).start(() => {
            this.setState({ currentIndex: currentIndex + 1 }, () => {
              this.position.setValue({ x: 0, y: 0 });
            });
          });
        } else {
          Animated.spring(this.position, {
            toValue: { x: 0, y: 0 },
            friction: 4,
          }).start();
        }
      },
    });
  }

  renderUsers = () => Match.map((item, i) => {
    const { currentIndex } = this.state;

    if (i < currentIndex) {
      return null;
    }
    if (i === currentIndex) {
      return (
        <Animated.View
          {...this.PanResponder.panHandlers}
          key={item.id}
          style={[this.rotateAndTranslate, {
            height: SCREEN_HEIGHT - 120, width: SCREEN_WIDTH, padding: 10, position: 'absolute',
          }]}
        >
          <Animated.View style={{
            opacity: this.likeOpacity, transform: [{ rotate: '-30deg' }], position: 'absolute', top: 50, left: 40, zIndex: 1000,
          }}
          >
            <Text style={{
              borderWidth: 1, borderColor: 'green', color: 'green', fontSize: 32, fontWeight: '800', padding: 10,
            }}
            >
LIKE
            </Text>

          </Animated.View>

          <Animated.View style={{
            opacity: this.dislikeOpacity, transform: [{ rotate: '30deg' }], position: 'absolute', top: 50, right: 40, zIndex: 1000,
          }}
          >
            <Text style={{
              borderWidth: 1, borderColor: 'red', color: 'red', fontSize: 32, fontWeight: '800', padding: 10,
            }}
            >
NOPE
            </Text>

          </Animated.View>

          <Image
            style={{
              flex: 1, height: null, width: null, resizeMode: 'cover', borderRadius: 20,
            }}
            source={item.uri}
          />

        </Animated.View>
      );
    }

    return (
      <Animated.View
        key={item.id}
        style={[{
          opacity: this.nextCardOpacity,
          transform: [{ scale: this.nextCardScale }],
          height: SCREEN_HEIGHT - 120,
          width: SCREEN_WIDTH,
          padding: 10,
          position: 'absolute',
        }]}
      >
        <Animated.View style={{
          opacity: 0, transform: [{ rotate: '-30deg' }], position: 'absolute', top: 50, left: 40, zIndex: 1000,
        }}
        >
          <Text style={{
            borderWidth: 1, borderColor: 'green', color: 'green', fontSize: 32, fontWeight: '800', padding: 10,
          }}
          >
LIKE
          </Text>

        </Animated.View>

        <Animated.View style={{
          opacity: 0, transform: [{ rotate: '30deg' }], position: 'absolute', top: 50, right: 40, zIndex: 1000,
        }}
        >
          <Text style={{
            borderWidth: 1, borderColor: 'red', color: 'red', fontSize: 32, fontWeight: '800', padding: 10,
          }}
          >
NOPE
          </Text>

        </Animated.View>

        <Image
          style={{
            flex: 1, height: null, width: null, resizeMode: 'cover', borderRadius: 20,
          }}
          source={item.uri}
        />

      </Animated.View>
    );
  }).reverse()

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: 60 }} />
        <View style={{ flex: 1 }}>
          {this.renderUsers()}
        </View>
        <View style={{ height: 60 }} />


      </View>

    );
  }
}
