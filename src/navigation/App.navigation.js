import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LoadingScreen from '../components/loadingScreen';
import HomeScreen from '../components/main/homeScreen';
import LoginScreen from '../components/auth/login';
import Profile from '../components/profile/profile';
import SignUpScreen from '../components/auth/signup';
import Matchs from '../components/profile/matchs';
import EditProfile from '../components/profile/editProfile';

const AppStack = createStackNavigator({
  Home: HomeScreen,
  Profile,
  EditProfile,
  Matchs,
});

const AuthStack = createStackNavigator({
  Login: LoginScreen,
  SignUp: SignUpScreen,
});

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      Loading: LoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'Loading',
    },
  ),
);

export default AppContainer;
