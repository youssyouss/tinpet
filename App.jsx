import React from 'react';
import { Provider } from 'react-redux';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import firebase from './src/config/firebase.config';
import configureStore from './src/redux/store';
import AppContainer from './src/navigation/App.navigation';

const initialState = {};
const store = configureStore(initialState);


const rrfConfig = {
  enableRedirectHandling: false, // required for react native
  userProfile: 'users', // where profiles are stored in database
  attachAuthIsReady: true, // attaches auth is ready promise to store
  useFirestoreForProfile: true,
  updateProfileOnLogin: false,
  logErrors: false,
};

const App = () => (
  <Provider store={store}>
    <ReactReduxFirebaseProvider
      firebase={firebase}
      config={rrfConfig}
      dispatch={store.dispatch}
    >
      <AppContainer />
    </ReactReduxFirebaseProvider>
  </Provider>
);

export default App;
